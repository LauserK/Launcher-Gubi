var gui = require('nw.gui'),
	path = require('path'),
	fs = require('fs'),
	http = require('http');

var mkdirp = require('mkdirp');
var request = require('request');
var progress = require('request-progress');

/*download = require('download-file');*/

var updateServerUrl = 'http://localhost:8000';

$(document).on('ready', mainFunction);

var createHash = function (archivo, callback) {
	var crypto = require('crypto');	
	var shasum = crypto.createHash('md5');

	var s = fs.ReadStream(archivo);
	s.on('data', function(d) { shasum.update(d); });
	s.on('end', function() {
	    var d = shasum.digest('hex');
	    $('#hash').text(d);

	    if (callback) {
	    	callback()
	    }
	});
}

function mainFunction() {

	$("#progressbar").progressbar({
		value: 0
	});

	$.getJSON(updateServerUrl+'/datos.json')
		.fail(function(err) {
			alert('Conection problem or server problems');
			console.log(err)
			gui.App.quit();
		})
		.done(function(data) {
			//Add ranking to the page
			var rank = 1;
			for (user in data.ranking) {
				$('.listRanking').append('#'+rank + ' ' + data.ranking[user].name + '<br>');
				rank = rank + 1;
			}

			//Start the update
			$('#btnUpdate').on('click', btnStart)
		})
}

function btnStart() {
	$('#btnUpdate').prop('disabled', true);
	$('#btnUpdate').text('Updating...')

	var fsToUpdate = new Array;

	$.getJSON(updateServerUrl+'/patch.json?v=35535')
		.done(function (data) {			
			for (var item = 0; item<data.items.length; item++) {				
				fsToUpdate.push(data.items[item])
			}		
			actualizarCliente(fsToUpdate);

		})
}

function actualizarCliente(fsToUpdate) {
	var nwPath = process.execPath;
	var nwDir = path.dirname(nwPath);

	//Set total files to download
	var totalfiles = Object.keys(fsToUpdate).length;
	$('.totalfiles').text(totalfiles);	
	$('.filesToDownload').text(totalfiles);


	for (var item=0; item<fsToUpdate.length; item++) {		
		file = fsToUpdate[item];		
		//Full path use for search local
		localFilePath = nwDir + "\\" + file.path+file.name;
		localPath = nwDir + "\\" + file.path;

		//Check if exists the folder, if not it create
		if (!fs.existsSync(localPath)) {
			mkdirp(file.path, function (err) {
			    if (err) console.error(err)
			    else console.log('Folder created!')
			});
		}

		//If file exists
		if (fs.existsSync(localFilePath)) {
        	//Create hash and put in hidden span .hashhelper
        	createHash(localFilePath, function() {
        		var md5Local = $('#hash').text();
		        var md5Server = file.md5;
		        var localFile = fs.statSync(localFilePath);		        

		        if (md5Local != md5Server || file.size != localFile["size"]) {
		        	//Delete old file		        		
	        		fs.unlinkSync(file.path+file.name);

	        		//Download new file
	        		DownloadFile(file.name, file.path, localFilePath);

	        		//Update counter of files to download
	        		totalfiles = totalfiles - 1;
	        		$('.totalfiles').text(totalfiles);
		        } else {
		        	console.log('Dont updated')
		        	//Update counter of files to download
	        		totalfiles = totalfiles - 1;
	        		$('.totalfiles').text(totalfiles);
		        }
        	});

        	$('#hash').text('')
		} else {
			console.log('no existe');
			DownloadFile(file.name, file.path, localFilePath);
			//Update counter of files to download
			totalfiles = totalfiles - 1;
			$('.totalfiles').text(totalfiles);
		}
	}

	console.log('Update finish');
	$('#btnUpdate').text('Play');
	$('#btnUpdate').attr("id","btnPlay")
	$('#btnPlay').prop('disabled', false);
	$('.finalmessage').text('Ready to Play');

}

function DownloadFile(fileName, directory, FileWPath) {
	
	//Change backslack for a normal slash
	directory = directory.replace("\\", "/");

	var url = updateServerUrl + '/' + directory + fileName;

	// Note that the options argument is optional 
	progress(request(url))
	.on('progress', function (state) {
		$("#progressbar").progressbar({
			value: state.percent
	    });

	    console.log('received size in bytes', state.received);
	    // The properties bellow can be null if response does not contain 
	    // the content-length header 
	    console.log('total size in bytes', state.total);
	    console.log('percent', state.percent);	   
	})
	.on('error', function (err) {
	    console.log(err);
	    console.log('Error downlading');
	})
	.pipe(fs.createWriteStream(FileWPath))
	.on('error', function (err) {
		console.log('Error save');
	    console.log(err.message);
	})
	.on('close', function (err) {		
	    console.log('File: ' + fileName + ' downloaded');
	})
}